from builtins import len
from flask import Flask, jsonify, request, render_template, session
from models.Usuario import Usuario
from models.Producto import Producto
from models.Rol import Rol
from models.Compra import Compra
from models.DetalleCompraProducto import DetalleCompraProducto
from models.DetalleVentaProducto import DetalleVentaProducto
from models.Proveedor import Proveedor
from models.Venta import Venta
from werkzeug.utils import secure_filename, redirect

from flask_bootstrap import Bootstrap
import os

# ----- App Configuration ----
app = Flask(__name__, template_folder='views')
Bootstrap(app)
app.secret_key = os.urandom(24)

@app.before_request
def session_management():
    session.permanent = True


@app.route('/home/')
def home():
    producto_ctrl = Producto()
    productos = producto_ctrl.getAll()
    return render_template('index.html', productos=productos)


# ---- Valores  ----
menu = None

# ------ Compra ---------
compra_ctrl = Compra()


@app.route('/compras/')
def homeCompra():
    return compra_ctrl.index(session['menu'])


@app.route('/compras/save', methods=['POST'])
def saveCompra():
    compra_data = compra_ctrl.save(request)
    if compra_data == True:
        output = dict(error=False, output=compra_data)
    else:
        output = dict(error=True, output='Compra no guardado')
    return output


@app.route('/compras/update', methods=['PUT'])
def updateCompra():
    compra_data = compra_ctrl.update(request)
    if compra_data == True:
        output = dict(error=False, output='Compra actualizada exitosamente')
    else:
        output = dict(error=True, output='Compra no actualizada')
    return output


@app.route('/compras/delete', methods=['DELETE'])
def deleteCompra():
    compra_data = compra_ctrl.delete(request)
    if compra_data == True:
        output = dict(error=False, output='Compra eliminada exitosamente')
    else:
        output = dict(error=True, output='Compra no eliminada')
    return output


@app.route('/compras/get', methods=['GET'])
def getCompra():
    compra_data = compra_ctrl.get(request)
    if compra_data != None:
        output = dict(error=False, output=compra_data)
    else:
        output = dict(error=True, output='Compra no encontrada')
    return output


@app.route('/compras/getHTML', methods=['GET'])
def getComprasHTML():
    compra_data = compra_ctrl.getAllHTML()
    if compra_data != None:
        output = dict(error=False, output=compra_data)
    else:
        output = dict(error=True, output='<span>No hay información relacionada</span>')
    return output


# ------ Detalle de la Compra ---------
detallecompra_ctrl = DetalleCompraProducto()


@app.route('/detallecompras/')
def homeDetalleCompra():
    return detallecompra_ctrl.index(session['menu'])


@app.route('/detallecompras/save')
def saveDetalleCompra():
    return detallecompra_ctrl.save(request)


@app.route('/detallecompras/get', methods=['GET'])
def getDetalleCompras():
    compra_data = detallecompra_ctrl.get(request)
    if compra_data != None:
        output = dict(error=False, output=compra_data)
    else:
        output = dict(error=True, output='Detalles de compra no encontrada')
    return output


# ------ Venta ---------
venta_ctrl = Venta()


@app.route('/ventas/')
def homeVenta():
    return venta_ctrl.index(session['menu'])


@app.route('/ventas/save', methods=['POST'])
def saveVenta():
    venta_data = venta_ctrl.save(request)
    if venta_data == True:
        output = dict(error=False, output=venta_data)
    else:
        output = dict(error=True, output='Venta no guardado')
    return output


@app.route('/ventas/update', methods=['PUT'])
def updateVenta():
    venta_data = venta_ctrl.update(request)
    if venta_data == True:
        output = dict(error=False, output='Venta actualizada exitosamente')
    else:
        output = dict(error=True, output='Venta no actualizada')
    return output


@app.route('/ventas/delete', methods=['DELETE'])
def deleteVenta():
    venta_data = venta_ctrl.delete(request)
    if venta_data == True:
        output = dict(error=False, output='Venta eliminada exitosamente')
    else:
        output = dict(error=True, output='Venta no eliminada')
    return output


@app.route('/ventas/get', methods=['GET'])
def getVenta():
    venta_data = venta_ctrl.get(request)
    if venta_data != None:
        output = dict(error=False, output=venta_data)
    else:
        output = dict(error=True, output='Venta no encontrada')
    return output


@app.route('/ventas/getHTML', methods=['GET'])
def getVentasHTML():
    venta_data = venta_ctrl.getAllHTML()
    if venta_data != None:
        output = dict(error=False, output=venta_data)
    else:
        output = dict(error=True, output='<span>No hay información relacionada</span>')
    return output


# ------ Detalle de la Venta ---------
detalleventa_ctrl = DetalleVentaProducto()


@app.route('/detalleventas/')
def homeDetalleVenta():
    return detalleventa_ctrl.index(session['menu'])


@app.route('/detalleventas/save')
def saveDetalleVenta():
    return detalleventa_ctrl.save(request)


@app.route('/detalleventas/get', methods=['GET'])
def getDetalleVentas():
    detalleventa_data = detalleventa_ctrl.get(request)
    if detalleventa_data != None:
        output = dict(error=False, output=detalleventa_data)
    else:
        output = dict(error=True, output='Detalles de venta no encontrada')
    return output


# ------ Proveedor ---------

proveedor_ctrl = Proveedor()


@app.route('/proveedores/')
def homeProveedor():
    return proveedor_ctrl.index(session['menu'])


@app.route('/proveedores/save', methods=['POST'])
def saveProveedor():
    proveedor_data = proveedor_ctrl.save(request)
    if proveedor_data == True:
        output = dict(error=False, output=proveedor_data)
    else:
        output = dict(error=True, output='Proveedor no guardado, revise información proporcionada')
    return output


@app.route('/proveedores/update', methods=['PUT'])
def updateProveedor():
    proveedor_data = proveedor_ctrl.update(request)
    if proveedor_data == True:
        output = dict(error=False, output='Proveedor actualizado exitosamente')
    else:
        output = dict(error=True, output='Proveedor no actualizado')
    return output


@app.route('/proveedores/delete', methods=['DELETE'])
def deleteProveedor():
    proveedor_data = proveedor_ctrl.delete(request)
    if proveedor_data == True:
        output = dict(error=False, output='Proveedor eliminado exitosamente')
    else:
        output = dict(error=True, output='Proveedor no eliminado')
    return output


@app.route('/proveedores/get', methods=['GET'])
def getProveedor():
    proveedor_data = proveedor_ctrl.get(request)
    if proveedor_data != None:
        output = dict(error=False, output=proveedor_data)
    else:
        output = dict(error=True, output='Proveedor no encontrado')
    return output


@app.route('/proveedores/getHTML', methods=['GET'])
def getProveedorHTML():
    proveedor_data = proveedor_ctrl.getAllHTML()
    if proveedor_data != None:
        output = dict(error=False, output=proveedor_data)
    else:
        output = dict(error=True, output='<span>No hay información relacionada</span>')
    return output


# ------ Usuarios ---------

usuario_ctrl = Usuario()


@app.route('/usuarios/')
def homeUsuario():
    return usuario_ctrl.index(session['menu'])


@app.route('/usuarios/save', methods=['POST'])
def saveUsuario():
    usuario_data = usuario_ctrl.save(request)
    if usuario_data == True:
        output = dict(error=False, output=usuario_data)
    else:
        output = dict(error=True, output='Usuario no guardado')
    return output


@app.route('/usuarios/update', methods=['PUT'])
def updateUsuario():
    usuario_data = usuario_ctrl.update(request)
    if usuario_data == True:
        output = dict(error=False, output='Usuario actualizado exitosamente')
    else:
        output = dict(error=True, output='Usuario no actualizado')
    return output


@app.route('/usuarios/delete', methods=['DELETE'])
def deleteUsuario():
    usuario_data = usuario_ctrl.delete(request)
    if usuario_data == True:
        output = dict(error=False, output='Usuario eliminado exitosamente')
    else:
        output = dict(error=True, output='Usuario no eliminado')
    return output


@app.route('/usuarios/get', methods=['GET'])
def getUsuario():
    usuario_data = usuario_ctrl.get(request)
    if usuario_data != None:
        output = dict(error=False, output=usuario_data)
    else:
        output = dict(error=True, output='Usuario no encontrado')
    return output


@app.route('/usuarios/getHTML', methods=['GET'])
def getUsuariosHTML():
    usuario_data = usuario_ctrl.getAllHTML()
    if usuario_data != None:
        output = dict(error=False, output=usuario_data)
    else:
        output = dict(error=True, output='<span>No hay información relacionada</span>')
    return output


@app.route('/login/', methods=['GET'])
def logIn():
    view = ''
    output = ''
    if request.method == 'GET':
        usuario_data = usuario_ctrl.login(request)
        if usuario_data != None:
            rol_ctrl = Rol()
            session.clear()
            session["menu"] = rol_ctrl.getMenu(usuario_data[0])
            session["usuario_session"] = usuario_data[0]
            session["info_usuario"] = usuario_data[3]
            session["rol_session"] = usuario_data[11]
            session['user_is_logged'] = True
            output = dict(error=False, output=usuario_data)
        else:
            output = dict(error=True, output='El usuario y la contraseña suministrada no son consistentes')
    return output


@app.route('/', methods=['GET'])
def inciarsesion():
    output = None
    if menu is not None:
        session['user_is_logged'] = False
    return render_template('LoginView.html', output=output)


@app.route('/destroy_session', methods=['GET'])
def destroysession():
    session.clear()
    output = None
    return render_template('LoginView.html', output=output)


# ----- Productos ---------
producto_ctrl = Producto()
@app.route('/productos/')
def homeProducto():
    return producto_ctrl.index(session['menu'])


@app.route('/productos/save', methods=['POST'])
def saveProducto():
    producto_data = producto_ctrl.save(request)
    if producto_data == True:
        output = dict(error=False, output=producto_data)
    else:
        output = dict(error=True, output='Producto no guardado')
    return output


@app.route('/productos/update', methods=['PUT'])
def updateProducto():
    producto_data = producto_ctrl.update(request)
    if producto_data == True:
        output = dict(error=False, output='Producto actualizado exitosamente')
    else:
        output = dict(error=True, output='Producto no actualizado')
    return output


@app.route('/productos/delete', methods=['DELETE'])
def deleteProducto():
    producto_data = producto_ctrl.delete(request)
    if producto_data == True:
        output = dict(error=False, output='Producto eliminado exitosamente')
    else:
        output = dict(error=True, output='Producto no eliminado')
    return output


@app.route('/productos/get', methods=['GET'])
def getProducto():
    producto_data = producto_ctrl.get(request)
    if producto_data != None:
        output = dict(error=False, output=producto_data)
    else:
        output = dict(error=True, output='Producto no encontrado')
    return output


@app.route('/productos/getHTML', methods=['GET'])
def getProductosHTML():
    producto_data = producto_ctrl.getAllHTML()
    if producto_data != None:
        output = dict(error=False, output=producto_data)
    else:
        output = dict(error=True, output='<span>No hay información relacionada</span>')
    return output


# -------- Exec --------

if __name__ == "__main__":
    app.run(debug=True)
