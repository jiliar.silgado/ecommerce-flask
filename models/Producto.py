import sqlite3

from models.Utils import Utils
from views.forms import ProductoForm as form
from flask import render_template, session, url_for
from datetime import datetime
from werkzeug.utils import secure_filename
import os

class Producto:

    def __init__(self):
        print("Producto Instanciado")

    def index(self, menu):
        data = form.ProductoForm()
        lista = self.getAll()
        cant_elements = 0
        if len(lista) > 0: cant_elements = len(lista[0]) + 1
        if session.get('menu') is not None:
            return render_template('ProductoView.html', form=[data, lista, cant_elements, menu])
        else:
            return render_template('LoginView.html')

    def save(self, request):
        info = form.ProductoForm()
        if request.method == 'POST':
            nombre = request.form['nombre']
            descripcion = request.form['descripcion']
            valor_compra = request.form['valor_compra']
            precio_venta = request.form['precio_venta']
            existencias = request.form['existencias']
            imagen = request.form['imagen']
            fecha_registro = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            usuario_registro = session.get('usuario_session')
            estado = 1
            file = request.files.getlist('imagen')[0]
            ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])
            ROOT_DIR = os.path.dirname(os.path.abspath(__file__))[:-6]
            filename = secure_filename(file.filename)
            try:
                if filename.rsplit('.')[1] in ALLOWED_EXTENSIONS:
                    file.save(os.path.join(ROOT_DIR+"\\views\\uploads\\images", filename))
                    utils = Utils()
                    imagen = utils.url+'static/uploads/images'+filename
                    with sqlite3.connect('db/ecommerceDB.db') as conexion:
                        cur = conexion.cursor()
                        query = 'INSERT INTO productos (nombre, descripcion, valor_compra, precio_venta, existencias, imagen, ' \
                                'fecha_registro, usuario_registro, estado) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)'
                        data = (nombre, descripcion, valor_compra, precio_venta, existencias, imagen, fecha_registro, usuario_registro, estado)
                        cur.execute(query, data)
                        conexion.commit()
                        return True
            except BaseException as e:
                return 'Error al intentar registrar Producto '+e.__str__()

    def get(self, request):
        if request.method == 'GET':
            rows = None
            try:
                with sqlite3.connect('db/ecommerceDB.db') as connection:
                    cur = connection.cursor()
                    query = 'SELECT * FROM productos WHERE estado = 1 AND id = ?'
                    cur.execute(query, (request.args['id']))
                    rows = cur.fetchone()
                return rows
            except BaseException as e:
                return 'Error al intentar obtener Producto ' + e.__str__()


    def update(self, request):
        info = form.ProductoForm()
        if request.method == 'PUT':
            id = request.form['id']
            nombre = request.form['nombre']
            descripcion = request.form['descripcion']
            valor_compra = request.form['valor_compra']
            precio_venta = request.form['precio_venta']
            existencias = request.form['existencias']
            file = request.files.getlist('imagen')[0]
            ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])
            ROOT_DIR = os.path.dirname(os.path.abspath(__file__))[:-6]
            filename = secure_filename(file.filename)
            try:
                if filename.rsplit('.')[1] in ALLOWED_EXTENSIONS:
                    file.save(os.path.join(ROOT_DIR+"\static\\uploads\images", filename))
                    utils = Utils()
                    imagen = utils.url+'/static/uploads/images/'+filename
                    with sqlite3.connect('db/ecommerceDB.db') as conexion:
                        cur = conexion.cursor()
                        query = 'UPDATE productos SET nombre = ?, descripcion = ?, valor_compra = ?, precio_venta = ?, existencias = ?, imagen = ? WHERE id = ?'
                        data = (nombre, descripcion, valor_compra, precio_venta, existencias, imagen, id)
                        cur.execute(query, data)
                        conexion.commit()
                        return True
                else:
                    return False
            except BaseException as e:
                return 'Error al intentar registrar Producto '+e.__str__()


    def delete(self, request):
        if request.method == 'DELETE':
            try:
                with sqlite3.connect('db/ecommerceDB.db') as conexion:
                    cur = conexion.cursor()
                    query = 'UPDATE productos SET estado = 2 WHERE id = ?'
                    cur.execute(query, (request.form['id']))
                    conexion.commit()
                    return True
            except BaseException as e:
                return 'Error al intentar registrar Producto '+e.__str__()

    def getAll(self):
        rows = None
        with sqlite3.connect('db/ecommerceDB.db') as connection:
            cur = connection.cursor()
            query = 'SELECT p.id AS ID, p.nombre AS NOMBRE, ' \
                    'p.descripcion AS DESCRIPCION, p.valor_compra AS VALOR_COMPRA, ' \
                    'p.precio_venta AS PRECIO_VENTA, p.existencias AS EXISTENCIAS, ' \
                    'p.imagen AS IMAGEN, p.fecha_registro AS FECHA_REGISTRO FROM productos p WHERE p.estado = 1'
            cur.execute(query)
            rows = cur.fetchall()
        return rows


    def getAllHTML(self):
        productos = self.getAll()
        data = form.ProductoForm()
        thead = "<thead id='thead-table'><tr>\n" \
             "<th style='text-align:center; font-family: 'Montserrat';'></th>\n" \
             "<th style='text-align:center;  font-family: 'Montserrat';'>Id</th>\n";
        th = "<th>{0}</th>\n"
        for d in data:
            if d.type != 'CSRFTokenField' and d.type != 'SubmitField':
                thead += th.format(d.label)
        thead += "</tr></thead>"
        tr = ""
        tdformat = "<td>{0}</td>\n"
        for values in productos:
            pos = 0
            tr += "<tr>"
            for val in values:
                if pos == 0:
                    tr += "<td style='text-align:center; font-family: 'Montserrat';'>" \
                          "<input class='selected form-check-input'" \
                          "type='checkbox' name='chxSeleccion_"+str(val)+"'" \
                           "id='chxSeleccion_" +str(val)+"'" \
                          "onclick='cbxAnalyze("+str(val)+")'>" \
                          "</td>\n"
                pos += 1
                if pos <= 10:
                    tr += tdformat.format(val)
            tr += "</tr>"
        tbody = "<tbody id='thead-table'>\n"+tr+"</tbody>"
        table = "<table id='info-table' class='table table-striped table-bordered' style='width:100%'>"\
                +thead+tbody+"</table>"
        return table