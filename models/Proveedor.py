import sqlite3
from views.forms import ProveedorForm as form
from flask import render_template, session
from datetime import datetime

class Proveedor:

    def __init__(self):
        print("Proveedor Instanciado")

    def index(self, menu):
        data = form.ProveedorForm()
        lista = self.getAll()
        cant_elements = 0
        if len(lista) > 0: cant_elements = len(lista[0]) + 1
        if session.get('menu') is not None:
            return render_template('ProveedorView.html', form=[data, lista, cant_elements, menu])
        else:
            return render_template('LoginView.html')

    def save(self, request):
        info = form.ProveedorForm()
        if request.method == 'POST':
            nit = request.form['nit']
            razon_social = request.form['razon_social']
            telefono = request.form['telefono']
            correo_electronico = request.form['correo_electronico']
            contacto = request.form['contacto']
            fecha_registro = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            usuario_registro = session.get('usuario_session')
            estado = 1
            try:
                with sqlite3.connect('db/ecommerceDB.db') as conexion:
                    cur = conexion.cursor()
                    query = 'INSERT INTO proveedores(nit, razon_social, telefono, correo_electronico, contacto, ' \
                            'fecha_registro, usuario_registro, estado) VALUES (?, ?, ?, ?, ?, ?, ?, ?)'
                    data = (nit, razon_social, telefono, correo_electronico, contacto, fecha_registro, usuario_registro, estado)
                    cur.execute(query, data)
                    conexion.commit()
                    return True
            except BaseException as e:
                return 'Error al intentar registrar Proveedor '+e.__str__()

    def get(self, request):
        if request.method == 'GET':
            rows = None
            try:
                with sqlite3.connect('db/ecommerceDB.db') as connection:
                    cur = connection.cursor()
                    query = 'SELECT * FROM proveedores WHERE estado = 1 AND id = ?'
                    cur.execute(query, (request.args['id']))
                    rows = cur.fetchone()
                return rows
            except BaseException as e:
                return 'Error al intentar obtener Proveedor ' + e.__str__()


    def update(self, request):
        if request.method == 'PUT':

            id = request.form['id']
            nit = request.form['nit']
            razon_social = request.form['razon_social']
            telefono = request.form['telefono']
            correo_electronico = request.form['correo_electronico']
            contacto = request.form['contacto']

            try:
                with sqlite3.connect('db/ecommerceDB.db') as conexion:
                    cur = conexion.cursor()
                    query = 'UPDATE proveedores SET nit = ?, razon_social = ?, telefono = ?, correo_electronico = ?, contacto = ? WHERE id = ?'
                    data = (nit, razon_social, telefono, correo_electronico, contacto, id)
                    cur.execute(query, data)
                    conexion.commit()
                    return True
            except BaseException as e:
                return 'Error al intentar registrar Usuario '+e.__str__()


    def delete(self, request):
        if request.method == 'DELETE':
            try:
                with sqlite3.connect('db/ecommerceDB.db') as conexion:
                    cur = conexion.cursor()
                    query = 'UPDATE proveedores SET estado = 2 WHERE id = ?'
                    cur.execute(query, (request.form['id']))
                    conexion.commit()
                    return True
            except BaseException as e:
                return 'Error al intentar registrar Usuario '+e.__str__()

    def getAll(self):
        rows = None
        with sqlite3.connect('db/ecommerceDB.db') as connection:
            cur = connection.cursor()
            query = 'SELECT p.id AS ID, p.nit AS NIT, p.razon_social AS RAZON_SOCIAL, ' \
                    'p.telefono AS TELEFONO, p.correo_electronico AS CORREO_ELECTRONICO, ' \
                    'p.contacto AS CONTACTO, p.fecha_registro AS FECHA_REGISTRO FROM proveedores p WHERE p.estado = 1'
            cur.execute(query)
            rows = cur.fetchall()
        return rows

    def getAllHTML(self):
        proveedores = self.getAll()
        data = form.ProveedorForm()
        thead = "<thead id='thead-table'><tr>\n" \
             "<th style='text-align:center; font-family: 'Montserrat';'></th>\n" \
             "<th style='text-align:center;  font-family: 'Montserrat';'>Id</th>\n";
        th = "<th>{0}</th>\n"
        for d in data:
            if d.type != 'CSRFTokenField' and d.type != 'SubmitField':
                thead += th.format(d.label)
        thead += "</tr></thead>"
        tr = ""
        tdformat = "<td>{0}</td>\n"
        for values in proveedores:
            pos = 0
            tr += "<tr>"
            for val in values:
                if pos == 0:
                    tr += "<td style='text-align:center; font-family: 'Montserrat';'>" \
                          "<input class='selected form-check-input'" \
                          "type='checkbox' name='chxSeleccion_"+str(val)+"'" \
                           "id='chxSeleccion_" +str(val)+"'" \
                          "onclick='cbxAnalyze("+str(val)+")'>" \
                          "</td>\n"
                pos += 1
                if pos <= 9:
                    tr += tdformat.format(val)
            tr += "</tr>"
        tbody = "<tbody id='thead-table'>\n"+tr+"</tbody>"
        table = "<table id='info-table' class='table table-striped table-bordered' style='width:100%'>"\
                +thead+tbody+"</table>"
        return table