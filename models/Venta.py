import sqlite3
from views.forms import VentaForm as form
from flask import render_template, session
from datetime import datetime

class Venta:

    def __init__(self):
        print("Venta Instanciada")

    def index(self, menu):
        data = form.VentaForm()
        lista = self.getAll()
        cant_elements = 0
        if len(lista) > 0 : cant_elements = len(lista[0]) + 1
        if session.get('menu') is not None:
            return render_template('VentaView.html', form=[data, lista, cant_elements, menu])
        else:
            return render_template('LoginView.html')

    def save(self, request):
        info = form.VentaForm()
        if request.method == 'POST':
            id_usuario = request.form['id_usuario']
            valor_total = request.form['valor_total']
            impuesto = request.form['impuesto']
            descuento = request.form['descuento']
            fecha_registro = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            usuario_registro = session.get('usuario_session')
            estado = 1
            try:
                with sqlite3.connect('db/ecommerceDB.db') as conexion:
                    cur = conexion.cursor()
                    query = 'INSERT INTO ventas (id_usuario, valor_total, impuestos, descuento, ' \
                            'fecha_registro, usuario_registro, estado) VALUES (?, ?, ?, ?, ?, ?, ?)'
                    data = (id_usuario, valor_total, impuesto, descuento, fecha_registro, usuario_registro, estado)
                    cur.execute(query, data)
                    conexion.commit()
                    return True
            except BaseException as e:
                return 'Error al intentar registrar la venta '+e.__str__()

    def get(self, request):
        if request.method == 'GET':
            rows = None
            try:
                with sqlite3.connect('db/ecommerceDB.db') as connection:
                    cur = connection.cursor()
                    query = 'SELECT * FROM ventas WHERE estado = 1 AND id = ?'
                    cur.execute(query, (request.args['id']))
                    rows = cur.fetchone()
                return rows
            except BaseException as e:
                return 'Error al intentar obtener la venta ' + e.__str__()


    def update(self, request):
        info = form.VentaForm()
        if request.method == 'PUT':
            id = request.form['id']
            id_usuario = request.form['id_usuario']
            valor_total = request.form['valor_total']
            impuesto = request.form['impuesto']
            descuento = request.form['descuento']
            try:
                with sqlite3.connect('db/ecommerceDB.db') as conexion:
                    cur = conexion.cursor()
                    query = 'UPDATE ventas SET id_usuario = ?, descripcion = ?, valor_total = ?, impuesto = ? WHERE id = ?'
                    data = (id_usuario, valor_total, impuesto, descuento, id)
                    cur.execute(query, data)
                    conexion.commit()
                    return True
            except BaseException as e:
                return 'Error al intentar actualizar la venta '+e.__str__()


    def delete(self, request):
        if request.method == 'DELETE':
            try:
                with sqlite3.connect('db/ecommerceDB.db') as conexion:
                    cur = conexion.cursor()
                    query = 'UPDATE ventas SET estado = 2 WHERE id = ?'
                    cur.execute(query, (request.form['id']))
                    conexion.commit()
                    return True
            except BaseException as e:
                return 'Error al intentar eliminar la venta '+e.__str__()

    def getAll(self):
        rows = None
        with sqlite3.connect('db/ecommerceDB.db') as connection:
            cur = connection.cursor()
            query = 'SELECT * FROM ventas WHERE estado = 1'
            cur.execute(query)
            rows = cur.fetchall()
        return rows


    def getAllHTML(self):
        ventas = self.getAll()
        data = form.VentaForm()
        thead = "<thead id='thead-table'><tr>\n" \
             "<th style='text-align:center; font-family: 'Montserrat';'></th>\n" \
             "<th style='text-align:center;  font-family: 'Montserrat';'>Id</th>\n";
        th = "<th>{0}</th>\n"
        for d in data:
            if d.type != 'CSRFTokenField' and d.type != 'SubmitField':
                thead += th.format(d.label)
        thead += "</tr></thead>"
        tr = ""
        tdformat = "<td>{0}</td>\n"
        for values in ventas:
            pos = 0
            tr += "<tr>"
            for val in values:
                if pos == 0:
                    tr += "<td style='text-align:center; font-family: 'Montserrat';'>" \
                          "<input class='selected form-check-input'" \
                          "type='checkbox' name='chxSeleccion_"+str(val)+"'" \
                           "id='chxSeleccion_" +str(val)+"'" \
                          "onclick='cbxAnalyze("+str(val)+")'>" \
                          "</td>\n"
                pos += 1
                if pos <= 8:
                    tr += tdformat.format(val)
            tr += "</tr>"
        tbody = "<tbody id='thead-table'>\n"+tr+"</tbody>"
        table = "<table id='info-table' class='table table-striped table-bordered' style='width:100%'>"\
                +thead+tbody+"</table>"
        return table